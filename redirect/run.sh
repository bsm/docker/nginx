#!/usr/bin/env sh

if [ ! -n "$REDIRECT_URL" ] ; then
  echo "Environment variable SERREDIRECT_URLVER_REDIRECT is not set, exiting."
  exit 1
fi

# set redirect code from optional ENV var
if [ ! -n "$REDIRECT_CODE" ] ; then
    REDIRECT_CODE='301'
fi

# set server name from optional ENV var
if [ ! -n "$SERVER_NAME" ] ; then
    SERVER_NAME='localhost'
fi

sed -i "s|\${REDIRECT_URL}|${REDIRECT_URL}|" /etc/nginx/conf.d/default.conf
sed -i "s|\${REDIRECT_CODE}|${REDIRECT_CODE}|" /etc/nginx/conf.d/default.conf
sed -i "s|\${SERVER_NAME}|${SERVER_NAME}|" /etc/nginx/conf.d/default.conf

exec nginx -g 'daemon off;'